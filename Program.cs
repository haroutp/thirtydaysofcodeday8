﻿using System;
using System.Collections.Generic;

namespace Day8
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> phoneBook = new Dictionary<string, int>();
            // System.Console.WriteLine(int.MaxValue + " " + long.MaxValue);
            int n = int.Parse(Console.ReadLine());

            for (int i = 0; i < n; i++)
            {
                string[] current = Console.ReadLine().Split(' ');
                phoneBook[current[0]] = Convert.ToInt32(current[1]);
            }

            for (int i = 0; i < n; i++)
            {
                var name = Console.ReadLine();
                if(phoneBook.ContainsKey(name)){
                    System.Console.WriteLine($"{name}={phoneBook[name]}");
                }else{
                    System.Console.WriteLine("Not found");
                }
            }
        }
    }
}
